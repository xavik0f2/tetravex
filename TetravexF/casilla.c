#include <stdio.h>
#include "colores.h"
#include "casilla.h"

/*En todas las funciones se ha replicado el mismo codigo para escoger el color por ser la version basica del juego,
 en la version completa crearemos una funcion extra para esa utilidad
 */
void imprimir_norte_casilla(t_casilla cas) {
    //Imprime las primeras barras para que no tengan color
    printf("\\");
    //caso en que la casillas estàn vacios
    if (cas.n == -1) {
        printf(" ");
        printf("/|");
        //Los if son para los colores de los diferentes numeros
    } else {
        if (cas.n == 1) {
            printf_color(1);
        }
        if (cas.n == 2) {
            printf_color(2);
        }
        if (cas.n == 3) {
            printf_color(3);
        }
        if (cas.n == 4) {
            printf_color(4);
        }
        if (cas.n == 5) {
            printf_color(5);
        }
        if (cas.n == 6) {
            printf_color(6);
        }
        if (cas.n == 7) {
            printf_color(7);
        }
        if (cas.n == 8) {
            printf_color(8);
        }
        if (cas.n == 9) {
            printf_color(9);
        }
        printf("%d", cas.n);
        //Se usa la funcion siguiente para que el color vuelva a ser default(blanco)
        printf_reset_color();
        //Se imprimen las ultimas para que no tengan color
        printf("/|");
    }

}

void imprimir_centro_casilla(t_casilla cas) {
    //caso en que las casillas estàn vacios
    if (cas.e == -1 && cas.o == -1) {
        printf(" ");
        printf("x");
        printf(" ");
        printf("|");
        //If para el color de la casilla oeste
    } else {
        if (cas.o == 1) {
            printf_color(1);
        }
        if (cas.o == 2) {
            printf_color(2);
        }
        if (cas.o == 3) {
            printf_color(3);
        }
        if (cas.o == 4) {
            printf_color(4);
        }
        if (cas.o == 5) {
            printf_color(5);
        }
        if (cas.o == 6) {
            printf_color(6);
        }
        if (cas.o == 7) {
            printf_color(7);
        }
        if (cas.o == 8) {
            printf_color(8);
        }
        if (cas.o == 9) {
            printf_color(9);
        }
        printf("%d", cas.o);
        printf_reset_color();
        printf("x");
        //If para las casillas este
        if (cas.e == 1) {
            printf_color(1);
        }
        if (cas.e == 2) {
            printf_color(2);
        }
        if (cas.e == 3) {
            printf_color(3);
        }
        if (cas.e == 4) {
            printf_color(4);
        }
        if (cas.e == 5) {
            printf_color(5);
        }
        if (cas.e == 6) {
            printf_color(6);
        }
        if (cas.e == 7) {
            printf_color(7);
        }
        if (cas.e == 8) {
            printf_color(8);
        }
        if (cas.e == 9) {
            printf_color(9);
        }
        printf("%d", cas.e);
        //Se vuelve al color default
        printf_reset_color();
        printf("|");
    }
}

void imprimir_sur_casilla(t_casilla cas) {
    //caso en que las casillas estàn vacios
    printf("/");
    if (cas.s == -1) {
        printf(" ");
        printf("\\|");
        //If para la casilla sur
    } else {
        if (cas.s == 1) {
            printf_color(1);
        }
        if (cas.s == 2) {
            printf_color(2);
        }
        if (cas.s == 3) {
            printf_color(3);
        }
        if (cas.s == 4) {
            printf_color(4);
        }
        if (cas.s == 5) {
            printf_color(5);
        }
        if (cas.s == 6) {
            printf_color(6);
        }
        if (cas.s == 7) {
            printf_color(7);
        }
        if (cas.s == 8) {
            printf_color(8);
        }
        if (cas.s == 9) {
            printf_color(9);
        }
        printf("%d", cas.s);
        //Se vuelve al color default
        printf_reset_color();
        printf("\\|");
    }
}

void intercambiar_casillas(t_casilla *cas1, t_casilla *cas2) {
    //Paso el valor de una casilla a otra con la ayuda de una variable auxiliar
    t_casilla cas_aux;

    cas_aux.co = cas2->co;
    cas_aux.fo = cas2->fo;
    cas_aux.n = cas2->n;
    cas_aux.s = cas2->s;
    cas_aux.e = cas2->e;
    cas_aux.o = cas2->o;

    cas2->co = cas1->co;
    cas2->fo = cas1->fo;
    cas2->n = cas1->n;
    cas2->s = cas1->s;
    cas2->e = cas1->e;
    cas2->o = cas1->o;

    cas1->co = cas_aux.co;
    cas1->fo = cas_aux.fo;
    cas1->n = cas_aux.n;
    cas1->s = cas_aux.s;
    cas1->e = cas_aux.e;
    cas1->o = cas_aux.o;

}

