#include <stdio.h>
#include "azar.h"
#include "tiempo.h"
#include "tablero.h"
#define TRUE 1
#define FALSE 0

void inicializar_tablero(t_tablero *tablero) {


    t_casilla c;
    int i, j;
    char tipo;


    //llamo a inicializar_azar() para que el tablero sea diferente cada vez
    inicializar_azar();


    //pido al usuario el tamano del tablero verificando que el valor sea correcto
    do {
        printf("Tamano tablero entre 2 y 6: ");
        scanf("%d%*c", &tablero->size);
    } while ((tablero->size != 2) && (tablero->size != 3) && (tablero->size != 4) && (tablero->size != 5) && (tablero->size != 6));

    //pido al usuario el tipo de tablero (simple o doble) verificando que el valor sea correcto
    do {
        printf("Tipo de tablero ([s]:Simple o [d]:Doble): ");
        scanf("%c%*c", &tipo);
    } while (tipo != 's' && tipo != 'd');
    //caso en que el tablero es simple
    if (tipo == 's') {

        //inicializo los campos size y tipo del tablero
        tablero->tipo = TABLERO_SIMPLE;

        tablero->max_f = tablero->size - 1;
        tablero->max_c = tablero->size - 1;

        //inicializo loc campos max_f y max_c en funcion de size
        c.fo = tablero->size;
        c.co = tablero->size;

        //Lleno la matriz de numeros al azar

        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {
                tablero->c[i][j].s = numero_al_azar(10);
                tablero->c[i][j].n = numero_al_azar(10);
                tablero->c[i][j].e = numero_al_azar(10);
                tablero->c[i][j].o = numero_al_azar(10);
                tablero->c[i][j].fo = i;
                tablero->c[i][j].co = j;
            }
        }


        //Inicializo todas la casillas de tal forma que sean un puzle resuelto al azar


        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {

                tablero->c[i][j].e = tablero->c[i][j + 1].o;
                tablero->c[i][j].s = tablero->c[i + 1][j].n;


                if (j == tablero->size - 1)
                    tablero->c[i][j].e = numero_al_azar(10);
                if (i == tablero->size - 1)
                    tablero->c[i][j].s = numero_al_azar(10);
            }
        }


        //mezclo el tablero

        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {


                intercambiar_casillas(&tablero->c[i][j], &tablero->c[numero_al_azar(tablero->size)][numero_al_azar(tablero->size)]);

            }
        }
        //caso en que el tablero es doble
    } else if (tipo == 'd') {

        //inicializo los campos tipo del tablero
        tablero->tipo = TABLERO_DOBLE;

        //inicializo los campos max_c y max_f, en este caso tengo el doble de columans menos uno
        tablero->max_f = tablero->size - 1;
        tablero->max_c = (tablero->size * 2) - 1;


        c.fo = tablero->size;
        c.co = tablero->size;



        //Lleno la matriz de numeros al azar
        //coloco la mitad ezquierda (hasta size-1) con numero aletaorio
        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {

                tablero->c[i][j].s = numero_al_azar(10);
                tablero->c[i][j].n = numero_al_azar(10);
                tablero->c[i][j].e = numero_al_azar(10);
                tablero->c[i][j].o = numero_al_azar(10);
                tablero->c[i][j].fo = i;
                tablero->c[i][j].co = j;
            }
        }
        //coloco la mitad derecha con casillas vacias (desde size hasta max_c)
        for (i = 0; i < tablero->size; i++) {
            for (j = tablero->size; j <= tablero->max_c; j++) {
                tablero->c[i][j].s = CASILLA_VACIA;
                tablero->c[i][j].n = CASILLA_VACIA;
                tablero->c[i][j].e = CASILLA_VACIA;
                tablero->c[i][j].o = CASILLA_VACIA;
                tablero->c[i][j].fo = i;
                tablero->c[i][j].co = j;

            }
        }

        //Inicializo todas la casillas de la mitad ezquierda (hasta size-1) de tal forma que sean un puzle resuelto al azar
        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {

                tablero->c[i][j].e = tablero->c[i][j + 1].o;
                tablero->c[i][j].s = tablero->c[i + 1][j].n;


                if (j == tablero->size - 1)
                    tablero->c[i][j].e = numero_al_azar(10);
                if (i == tablero->size - 1)
                    tablero->c[i][j].s = numero_al_azar(10);
            }
        }
       
        //mezclo el tablero
        //mezclo solamente la mitad ezquierda del tablero (hasta size-1)
        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {

                //intercambio la casillas en forma aleatoria pero que no exceda el tamaño de la tabla i que no intercambi la misma
                intercambiar_casillas(&tablero->c[i][j], &tablero->c[numero_al_azar(tablero->size)][numero_al_azar(tablero->size)]);
            }
        }



        // pongo la mitad ezquierda en la mitad derecha, intercambiando las casillas
        for (i = 0; i < tablero->size; i++) {
            for (j = 0; j < tablero->size; j++) {
                //las filas sigue siendo la misma, cambo sólo las columnas
                intercambiar_casillas(&tablero->c[i][j], &tablero->c[i][j + tablero->size]);
            }
        }

    }
    //Empieza a contar el tiempo hasta que se realice una jugada
    empieza_tiempo();
    //Inicializo el numero de pistas
    tablero->pistas = (tablero->size * tablero->size) / 5;
}

void imprimir_tablero(t_tablero tablero) {

    int i, j, k;
    t_casilla c;
    int time = tiempo_transcurrido();

    // caso en que el tablero es simple
    if (tablero.tipo == TABLERO_SIMPLE) {
        printf("\n");
        //Imprimir letra de la casilla
        for (j = 0; j < tablero.size; j++)
            //El if porque la primera letra tendra 4 espacios del principio de linea 
            if (j < 1) {
                printf("    %c", 'A' + j);
                //else porque cada letra esta separada de ella 3 espacios
            } else {
                printf("   %c", 'A' + j);
            }
        printf("\n");
        //Imprimir marco del tablero
        //Primer marco de las casillas que separa las letras con las casillas norte
        //Se imprime uno fuera del bucle porque el primero esta separado 2 espacios del principio de linea
        printf("  +---");
        for (k = 1; k < tablero.size; k++) {
            if (k < (tablero.size - 1)) {
                printf("+---");
            } else {
                printf("+---+");
            }
        }
        printf("\n");
        //Imprimir casillas
        for (i = 0; i < tablero.size; i++) {
            //Norte
            printf("  |");
            for (j = 0; j < tablero.size; j++) {

                imprimir_norte_casilla(tablero.c[i][j]);

            }
            printf("\n");
            //Imprimir numero de la casilla
            printf(" %d", i);
            //Centro
            printf("|");
            for (j = 0; j < tablero.size; j++) {

                imprimir_centro_casilla(tablero.c[i][j]);
            }
            printf("\n");
            //Sur
            printf("  |");
            for (j = 0; j < tablero.size; j++) {

                imprimir_sur_casilla(tablero.c[i][j]);

            }
            printf("\n");
            //Ultimo marco del tablero que separa las casillas sur i las norte
            printf("  +---");
            for (k = 1; k < tablero.size; k++) {
                if (k < (tablero.size - 1)) {
                    printf("+---");
                } else {
                    printf("+---+");
                }
            }
            printf("\n");
        }

        //caso en que el tablero es doble
    } else if (tablero.tipo == TABLERO_DOBLE) {

        printf("\n");
        //Imprimir letra de la casilla
        for (j = 0; j <= tablero.max_c; j++)
            //El if porque la primera letra tendra 4 espacios del principio de linea 
            if (j < 1) {
                printf("    %c", 'A' + j);
                //else porque cada letra esta separada de ella 3 espacios
            } else {
                printf("   %c", 'A' + j);
            }
        printf("\n");
        //Imprimir marco del tablero
        //Primer marco de las casillas que separa las letras con las casillas norte
        //Se imprime uno fuera del bucle porque el primero esta separado 2 espacios del principio de linea
        printf("  +---");
        for (k = 1; k <= tablero.max_c; k++) {
            if (k < (tablero.max_c)) {
                printf("+---");
            } else {
                printf("+---+");
            }
        }
        printf("\n");
        //Imprimir casillas
        for (i = 0; i < tablero.size; i++) {
            //Norte
            printf("  |");
            for (j = 0; j <= tablero.max_c; j++) {

                imprimir_norte_casilla(tablero.c[i][j]);

            }
            printf("\n");
            //Imprimir numero de la casilla
            printf(" %d", i);
            //Centro
            printf("|");
            for (j = 0; j <= tablero.max_c; j++) {

                imprimir_centro_casilla(tablero.c[i][j]);
            }
            printf("\n");
            //Sur
            printf("  |");
            for (j = 0; j <= tablero.max_c; j++) {

                imprimir_sur_casilla(tablero.c[i][j]);

            }
            printf("\n");
            //Ultimo marco del tablero que separa las casillas sur i las norte
            printf("  +---");
            for (k = 1; k <= tablero.max_c; k++) {
                if (k < (tablero.max_c)) {
                    printf("+---");
                } else {
                    printf("+---+");
                }
            }
            printf("\n");
        }
    }
    printf("\n");


    //Imprime el tiempo en este caso segundos, .2 para que imprima 00 como en un reloj digital
    if (time < 60) {
        printf("[%.2dh:%.2dm:%.2ds]", time / 3600, time / 60, time);
    }
    //Imprime los minutos donde los segundos son el resto de los minutos/60
    if (time >= 60 && time < 3600) {
        printf("[%.2dh:%.2dm:%.2ds]", time / 3600, time / 60, time % 60);

    }
    //Imprime las horas donde los minutos es el resto de horas/3600 son segundos , ese resto /60 son minutos, y el modulo son los segundos
    if (time >= 3600) {
        printf("[%.2dh:%.2dm:%.2ds]", (time / 3600), ((time % 3600) / 60), ((time % 3600) % 60));

    }
    printf("\n");
    printf("\n");
}

void realizar_jugada(t_tablero *tablero) {

    int i, j;
    int i2, j2;
    t_casilla aux1, aux2;
    int y1, y2;
    char x1, x2;
    printf("\nCasillas a intercambiar (ej: [A0B1]):");
    scanf("%c%d%c%d%*c", &x1, &y1, &x2, &y2);
    if ((x1 == 'z' || x1 == 'Z') && (x2 == 'z' || x2 == 'Z')) {
        if (tablero->tipo == TABLERO_SIMPLE) {

            for (i = 0; i < tablero->size; i++)
                for (j = 0; j < tablero->size; j++) {
                    {
                        aux1 = tablero->c[i][j];

                        printf("%c%d:\n", j + 'A', i);
                        for (i2 = 0; i2 < tablero->size; i2++)
                            for (j2 = 0; j2 < tablero->size; j2++) {
                                {

                                    aux2 = tablero->c[aux1.fo][aux1.co];

                                    intercambiar_casillas(&tablero->c[aux1.fo][aux1.co], &tablero->c[aux2.fo][aux2.co]);
                                    if (!esta_resuelto(*tablero)) {

                                        printf("\t%c%d <--> %c%d\n", j + 'A', i, aux1.co + 'A', aux1.fo);
                                        imprimir_tablero(*tablero);
                                    }
                                }
                            }



                    }
                }
        } else if (tablero->tipo == TABLERO_DOBLE) {

            for (i = 0; i < tablero->size; i++)
                for (j = tablero->size; j <= tablero->max_c; j++) {
                    {
                        aux1 = tablero->c[i][j];
                        aux2 = tablero->c[aux1.fo][aux1.co];
                        intercambiar_casillas(&tablero->c[aux1.fo][aux1.co], &tablero->c[i][j]);
                        printf("%c%d:\n", j + 'A', i);
                        printf("\t%c%d <--> %c%d\n", j + 'A', i, aux1.co + 'A', aux1.fo);
                        imprimir_tablero(*tablero);

                    }
                }


        }
    } else {
        //Estos if y else serviran para ver que el usuario pone correctamente las casillas
        if (tablero->tipo == TABLERO_SIMPLE) {
            if (x1 <= tablero->size + 'A' && x1 >= 'A' && y1 <= tablero->size && y1 >= 0 && x2 <= tablero->size + 'A' && x2 >= 'A' && y2 <= tablero->size && y2 >= 0) {
            }//Si las casillas estan bien pero la letra en minuscula lo transforma a mayuscula i no pide de nuevo las casillas
            else if (x1 >= 'a' && x1 <= tablero->size + 'a' && y1 <= tablero->size && y1 >= 0 && x2 <= tablero->size + 'a' && x2 >= 'a' && y2 <= tablero->size && y2 >= 0) {
                //Pasar de minusculas a mayusculas
                x1 = x1 - 'a' + 'A';
                x2 = x2 - 'a' + 'A';
            } else if (x1 >= 'a' && x1 <= tablero->size + 'a' && y1 <= tablero->size && y1 >= 0 && x2 <= tablero->size + 'A' && x2 >= 'A' && y2 <= tablero->size && y2 >= 0) {
                x1 = x1 - 'a' + 'A';
            } else if (x1 >= 'A' && x1 <= tablero->size + 'A' && y1 <= tablero->size && y1 >= 0 && x2 <= tablero->size + 'a' && x2 >= 'a' && y2 <= tablero->size && y2 >= 0) {
                x2 = x2 - 'a' + 'A';
            } else {
                printf("\nCasillas a intercambiar (ej: [A0B1]):");
                scanf("%c%d%c%d%*c", &x1, &y1, &x2, &y2);
            }
        }
        //Hacemos if en funcion del tipo de tablero porque el numero de columnas no es el mismo segun el tipo 
        if (tablero->tipo == TABLERO_DOBLE) {
            if (x1 <= (tablero->size * 2) + 'A' && x1 >= 'A' && y1 <= tablero->size && y1 >= 0 && x2 <= (tablero->size * 2) + 'A' && x2 >= 'A' && y2 <= tablero->size && y2 >= 0) {
            } else if (x1 >= 'a' && x1 <= (tablero->size * 2) + 'a' && y1 <= tablero->size && y1 >= 0 && x2 <= (tablero->size * 2) + 'a' && x2 >= 'a' && y2 <= tablero->size && y2 >= 0) {

                x1 = x1 - 'a' + 'A';
                x2 = x2 - 'a' + 'A';
            } else if (x1 >= 'a' && x1 <= (tablero->size * 2) + 'a' && y1 <= tablero->size && y1 >= 0 && x2 <= (tablero->size * 2) + 'A' && x2 >= 'A' && y2 <= tablero->size && y2 >= 0) {
                x1 = x1 - 'a' + 'A';
            } else if (x1 >= 'A' && x1 <= (tablero->size * 2) + 'A' && y1 <= tablero->size && y1 >= 0 && x2 <= (tablero->size * 2) + 'a' && x2 >= 'a' && y2 <= tablero->size && y2 >= 0) {
                x2 = x2 - 'a' + 'A';
            } else {
                printf("\nCasillas a intercambiar (ej: [A0B1]):");
                scanf("%c%d%c%d%*c", &x1, &y1, &x2, &y2);
            }
        }//Se activan las pistas y se mira antes que tenga pistas
        else if (x1 == x2 && y1 == y2) {


            if (tablero->pistas < 1) {
                printf("No te quedan pistas");
            }//Analizara la casilla por ejemplo A0 i mirara los numeros 
            else if (tablero->c[y1][x1 - 'A'].n == CASILLA_VACIA && tablero->c[y1][x1 - 'A'].s == CASILLA_VACIA && tablero->c[y1][x1 - 'A'].e == CASILLA_VACIA && tablero->c[y1][x1 - 'A'].o == CASILLA_VACIA) {
                printf("Posicion correcta de %c%d", x1, y1);
                printf(" -> Esta casilla esta vacia! :-?\n");
                tablero->pistas--;
                printf("Te quedan %d pistas", tablero->pistas);
                printf("\n");

            } else if (tablero->pistas > 0) {
                //Indica la posicion correcta de la casilla a cambiar
                printf("Posicion correcta de %c%d", x1, y1);
                printf(" -> %c%d", tablero->c[y1][x1 - 'A'].co + 'A', tablero->c[y1][x1 - 'A'].fo);

                tablero->pistas--;
                printf("\n");
                //Imprime el numero de pistas que le quedan al usuario, si no le quedan ya no lo muestra
                printf("Te quedan %d pistas", tablero->pistas);
                printf("\n");
            }

        }
    }
    //Se toma los valores del usuario y se los intercambia como ha pedido
    intercambiar_casillas(&tablero->c[y1][x1 - 'A'], &tablero->c[y2][x2 - 'A']);

}

int esta_resuelto(t_tablero tablero) {


    int i, j, resuelto = 1;

    // contolo que todas las casillas están en el lugar correcto
    for (i = 0; i < tablero.size; i++) {
        for (j = 0; j < tablero.size; j++) {

            if (j != tablero.size - 1 && i != tablero.size - 1) {
                if ((tablero.c[i][j].s == tablero.c[i + 1][j].n) && (tablero.c[i][j].e == tablero.c[i][j + 1].o)) {

                    // in esta manera si todas las camparaciones devolven 1, el tablero esta resuelto (resuelto = 1).

                    resuelto *= TRUE;
                } else {

                    // si una sola comparacion devolve 0, el resultato serìa 0.
                    resuelto = FALSE;
                }

                // no necesito controlar la ultima fila y la ultima columna
            } else if (j == tablero.size - 1 && i != tablero.size - 1) {
                if (tablero.c[i][j].s == tablero.c[i + 1][j].n) {
                    resuelto *= TRUE;
                } else {
                    resuelto = FALSE;
                }
            } else if (i == tablero.size - 1 && j != tablero.size - 1) {
                if (tablero.c[i][j].e == tablero.c[i][j + 1].o) {
                    resuelto *= TRUE;
                } else {
                    resuelto = FALSE;
                }
                // si la casillas estàn vacios (-1), no se debe comprobar si se resuelve
            } else if ((tablero.c[i][j].e == -1) && (tablero.c[i][j].o == -1) && (tablero.c[i][j].n == -1) && (tablero.c[i][j].s == -1)) {
                resuelto = FALSE;
            }
        }
    }
    return resuelto;
}